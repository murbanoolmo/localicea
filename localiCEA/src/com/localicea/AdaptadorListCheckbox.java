/**
 *  Copyright(c) 2012 by María Urbano Olmo <urbanolmo@gmail.com>
 *
 * localiCEA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * localiCEA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with localiCEA. If not, see <http://www.gnu.org/licenses/>.
 */

package com.localicea;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

 //Esta clase extiende de AdaptadorListCheckbox para poder personalizarla a nuestro gusto 
public class AdaptadorListCheckbox extends ArrayAdapter<Check_Item> {

	Activity contexto;
	ArrayList<Check_Item> lista_items;

	//Constructor del AdaptadorListCheckbox donde se le pasaran por parametro el contexto de la aplicacion y el ArrayList 
	public AdaptadorListCheckbox(Activity context, ArrayList<Check_Item> lista) {
		//Llamada al constructor de la clase superior donde requiere el contexto, el layout y el arraylist
		super(context, R.layout.checkbox_item, lista);
		this.contexto = context;
		this.lista_items = lista;
	}

	//Este metodo es el que se encarga de dibujar cada Item de la lista
	//y se invoca cada vez que se necesita mostrar un nuevo item.
	//En el se pasan parametros como la posicion del elemento mostrado, la vista (View) del elemento mostrado y el conjunto de vistas
	public View getView(int position, View convertView, ViewGroup parent) {
		View item = convertView;

		//Creamos esta variable para almacen posteriormente en el la vista que ha dibujado
		VistaItem vistaitem;

		//Si se decide que no existe una vista reutilizable para el proximo item entra en la condicion.
		//De este modo tambien ahorramos tener que volver a generar vistas
		if (item == null) {

			//Obtenemos una referencia de Inflater para poder inflar el diseño
			LayoutInflater inflador = contexto.getLayoutInflater();

			//Se le define a la vista (item) el tipo de diseño que tiene que tener
			item = inflador.inflate(R.layout.checkbox_item, null);

			//Creamos un nuevo vistaitem que se almacenara en el tag de la vista
			vistaitem = new VistaItem();

			//Almacenamos en el objeto la referencia del TextView buscandolo por ID
			vistaitem.nombre = (TextView) item.findViewById(R.id.txt);

			//tambien almacenamos en el objeto la referencia del CheckBox buscandolo por ID
			vistaitem.chkEstado = (CheckBox) item.findViewById(R.id.chkEstado);

			//Ahora si, guardamos en el tag de la vista el objeto vistaitem 
			item.setTag(vistaitem);

		} else {
			//En caso de que la vista sea ya reutilizable se recupera el objeto VistaItem almacenada en su tag
			vistaitem = (VistaItem) item.getTag();
		}

		//Se cargan los datos desde el ArrayList
		vistaitem.nombre.setText(lista_items.get(position).getTexto());
		if(lista_items.get(position).isChekeado()==1)
			vistaitem.chkEstado.setChecked(true);
		else
			vistaitem.chkEstado.setChecked(false);
		
		//Se devuelve ya la vista nueva o reutilizada que ha sido dibujada
		return (item);
	}

	//Esta clase se usa para almacenar el TextView y el CheckBox de una vista y es donde esta el "truco" para que las vistas se guarden
	static public class VistaItem {
		TextView nombre;
		CheckBox chkEstado;
	}
}