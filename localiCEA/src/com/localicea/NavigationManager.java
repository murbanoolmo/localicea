/**
 *  Copyright(c) 2012 by María Urbano Olmo <urbanolmo@gmail.com>
 *
 * localiCEA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * localiCEA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with localiCEA. If not, see <http://www.gnu.org/licenses/>.
 */

package com.localicea;

import java.util.ArrayList;

import com.localicea.actividades.*;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class NavigationManager
{
	/**
	 * Navegamos a la actividad principal. Pantalla para seleccionar provincias.
	 * */ 
	public static void navegarAProvinciasActivity(Activity activity)
	{
		  activity.finish();
		  Intent itemintent = new Intent(activity, ProvinciasActivity.class);
		  activity.startActivity(itemintent);
	}
	
	/**
	 * Navegamos a la segunda actividad. Pantalla para seleccionar municipios.
	 * */ 	 
	public static void navegarAMunicipiosActivity(Activity activity, String provincia)
	{
		//Terminamos la actividad desde la que se comienza la navegación
		activity.finish();    
		//Creamos la actividad correspondiente a la segunda pantalla, Municipios
		Intent itemintent = new Intent(activity, MunicipiosActivity.class);
		//Creamos un objeto en el que volcaremos todos los parámetros que le queremos
		//pasar a la nueva actividad
		Bundle b = new Bundle();
		//en este caso, tan sólo le pasamos la provincia seleccionada, para mostrar sus municipios
		b.putString("provincia", provincia);
		//guardamos el Bundle en el Intent asociado a la actividad
		itemintent.putExtra("android.intent.extra.INTENT", b);
		//y por útlimo lanzamos la actividad
		activity.startActivity(itemintent);
	}
	
	/**
	 * Navegamos a la tercera actividad. Pantalla para seleccionar enseñanzas.
	 * */ 
	public static void navegarAEnsenianzasActivity(Activity activity, String provincia, String municipio,
													  ArrayList<Check_Item> lista_ensenianzas)
	{
		  activity.finish();    
		  Intent itemintent = new Intent(activity, EnsenianzaActivity.class);
		  Bundle b = new Bundle();
		  Bundle b2 = new Bundle();
		  b.putString("provincia", provincia);
		  b.putString("municipio", municipio);
		  b2.putParcelableArrayList("ensenianzas", lista_ensenianzas);
		  b.putBundle("bundleEns", b2);
		  itemintent.putExtra("android.intent.extra.INTENT", b);
		  activity.startActivity(itemintent);
	}
	
	/**
	 * Navegamos a la cuarta actividad. Pantalla para seleccionar servicios y programas.
	 * */ 
	public static void navegarAServiciosYProgramasActivity(Activity activity, 
			String provincia, String municipio,
			ArrayList<Check_Item> lista_ensenianzas,
			ArrayList<Check_Item> lista_servicios,
			ArrayList<Check_Item> lista_programas)
	{		
		  activity.finish();    
		  Intent itemintent = new Intent(activity, ServiciosYProgramasActivity.class);
		  Bundle b = new Bundle();
		  Bundle b2 = new Bundle();
		  b.putString("provincia", provincia);
		  b.putString("municipio", municipio);
		  b2.putParcelableArrayList("ensenianzas", lista_ensenianzas);
		  b2.putParcelableArrayList("servicios", lista_servicios);
		  b2.putParcelableArrayList("programas", lista_programas);
		  b.putBundle("bundleEns", b2);
		  itemintent.putExtra("android.intent.extra.INTENT", b);
		  activity.startActivity(itemintent);
	}
	
	/**
	 * Navegamos a la quinta actividad. Pantalla para visualizar el mapa con los centros resultantes de la búsqueda.
	 * */ 
	public static void navegarAMapaActivity(Activity activity,
			String provincia, String municipio,
			ArrayList<Check_Item> lista_ensenianzas,
			ArrayList<Check_Item> lista_servicios,
			ArrayList<Check_Item> lista_programas,
			Boolean todosLosRequisitos)
	{		
		  activity.finish();    
		  Intent itemintent = new Intent(activity, MapaActivity.class);
		  Bundle b = new Bundle();
		  Bundle b2 = new Bundle();
		  b.putString("provincia", provincia);
		  b.putString("municipio", municipio);
		  b.putBoolean("todosLosRequisitos", todosLosRequisitos);
		  b2.putParcelableArrayList("ensenianzas", lista_ensenianzas);
		  b2.putParcelableArrayList("servicios", lista_servicios);
		  b2.putParcelableArrayList("programas", lista_programas);
		  b.putBundle("bundleEns", b2);
		  itemintent.putExtra("android.intent.extra.INTENT", b);
		  activity.startActivity(itemintent);
	}
	
	/**
	 * Recuperamos todos los requisitos establecidos
	 * */
	public static Boolean getTodosLosRequisitos(Activity activity)
	{
		  Boolean todosLosRequisitos=false;
		  Intent startingIntent = activity.getIntent();
	      
		  if (startingIntent != null)
	      {
		         Bundle b = startingIntent.getBundleExtra("android.intent.extra.INTENT");
		         if (b != null) {
		        	 todosLosRequisitos = b.getBoolean("todosLosRequisitos");          
		         }
		  }
		  
	      return todosLosRequisitos;
	}
	
	/**
	 * Recuperamos la provincia seleccionada
	 * */	 
	public static String getProvincia(Activity activity)
	{
		  String provincia="";
		  Intent startingIntent = activity.getIntent();
	      
		  if (startingIntent != null)
	      {
		         Bundle b = startingIntent.getBundleExtra("android.intent.extra.INTENT");
		         if (b != null) {
		        	 provincia = b.getString("provincia");          
		         }
		  }
		  
	      return provincia;
	}
	
	/**
	 * Recuperamos el municipio seleccionado
	 * */
	public static String getMunicipio(Activity activity)
	{
		  String municipio="";
		  Intent startingIntent = activity.getIntent();
	      
		  if (startingIntent != null)
	      {
		         Bundle b = startingIntent.getBundleExtra("android.intent.extra.INTENT");
		         if (b != null) {
		        	 municipio = b.getString("municipio");          
		         }
		  }
		  
	      return municipio;
	}
	
	/**
	 * Recuperamos las enseñanzas seleccionadas
	 * */
	public static ArrayList<Check_Item> getEnsenianzas(Activity activity)
	{
			ArrayList<Check_Item> lista_ensenianzas=new ArrayList<Check_Item>();
			Intent startingIntent = activity.getIntent();
	      
			if (startingIntent != null)
			{
				 Bundle b = startingIntent.getBundleExtra("android.intent.extra.INTENT");
		         Bundle b2 = b.getBundle("bundleEns");
		         if (b2 != null) {
			         lista_ensenianzas = b2.getParcelableArrayList("ensenianzas");
		         }
			}
		  
			return lista_ensenianzas;
	}
	
	/**
	 * Recuperamos los servicios seleccionados
	 * */
	public static ArrayList<Check_Item> getServicios(Activity activity)
	{
			ArrayList<Check_Item> lista_servicios=new ArrayList<Check_Item>();
			Intent startingIntent = activity.getIntent();
	      
			if (startingIntent != null)
			{
				 Bundle b = startingIntent.getBundleExtra("android.intent.extra.INTENT");
		         Bundle b2 = b.getBundle("bundleEns");
		         if (b2 != null) {
		        	 lista_servicios = b2.getParcelableArrayList("servicios");
		         }
			}
		  
			return lista_servicios;
	}
	
	/**
	 * Recuperamos los programas seleccionados
	 * */
	public static ArrayList<Check_Item> getProgramas(Activity activity)
	{
			ArrayList<Check_Item> lista_programas=new ArrayList<Check_Item>();
			Intent startingIntent = activity.getIntent();
	      
			if (startingIntent != null)
			{
				 Bundle b = startingIntent.getBundleExtra("android.intent.extra.INTENT");
		         Bundle b2 = b.getBundle("bundleEns");
		         if (b2 != null) {
		        	 lista_programas = b2.getParcelableArrayList("programas");
		         }
			}
		  
			return lista_programas;
	}
}
