/**
 *  Copyright(c) 2012 by María Urbano Olmo <urbanolmo@gmail.com>
 *
 * localiCEA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * localiCEA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with localiCEA. If not, see <http://www.gnu.org/licenses/>.
 */

package com.localicea;

import android.os.Parcel;
import android.os.Parcelable;

public class Check_Item implements Parcelable {
	private String texto;
	//la variable estado no la ponemos boolean porque no existe para Parcelable el método writeBoolean.
	private int estado;
	
	
	//CONSTRUCTOR DE LA CLASE//
	public Check_Item(String txt, int estado) {
		this.texto = txt;
		this.estado = estado;
	}
	
    private Check_Item(Parcel in){
	        texto = in.readString();
	        estado = in.readInt();
	}
    
	public static final Parcelable.Creator<Check_Item> CREATOR =
	            new Parcelable.Creator<Check_Item>() {
	 
	        @Override
	        public Check_Item createFromParcel(Parcel source) {
	            return new Check_Item(source);
	        }
	 
	        @Override
	        public Check_Item[] newArray(int size) {
	            return new Check_Item[size];
	        }
	 
	 };

	//GETTERS Y SETTERS DE LA CLASE//
	
	public String getTexto() {
		return texto;
	}

	public void setTexto(String txt) {
		texto = txt;
	}

	public int isChekeado() {
		return estado;
	}

	public void setChekeado(int chekeado) {
		estado = chekeado;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(texto);
		dest.writeInt(estado);
	}
}
