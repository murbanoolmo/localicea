/**
 *  Copyright(c) 2012 by María Urbano Olmo <urbanolmo@gmail.com>
 *
 * localiCEA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * localiCEA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with localiCEA. If not, see <http://www.gnu.org/licenses/>.
 */

package com.localicea;

public class Centro_Item {
	private String codigo;
	private String denominacion;
	private String nombre;
	private String dependencia;
	private String domicilio;
	private String municipio;
	private String provincia;
	private String codPostal;
	private String telefono;
	private String ensenianzas;
	private String servicios;
	private String programas;
	private Float latitud;
	private Float longitud;
	
	public Centro_Item(String codigo, String denominacion, String nombre, String dependencia, String domicilio,
			String municipio, String provincia, String codPostal,String telefono, String ensenianzas,
			String servicios, String programas, String latitud, String longitud)
	{
		this.codigo = codigo;
		this.denominacion = denominacion;
		this.nombre = nombre;
		this.dependencia = dependencia; 
		this.domicilio = domicilio;
		this.municipio = municipio;
		this.provincia = provincia;
		this.codPostal = codPostal; 
		this.telefono = telefono; 
		this.ensenianzas = ensenianzas; 
		this.servicios = servicios; 
		this.programas = programas;
		this.latitud = new Float(latitud); 
		this.longitud = new Float(longitud); 
	}
	
	//Setters
	public void setCodigo(String codigo){ this.codigo = codigo; }
	public void setDenominacion(String denominacion){ this.denominacion = denominacion; }
	public void setNombre(String nombre){ this.nombre = nombre; }
	public void setDependencia(String dependencia){ this.dependencia = dependencia; }
	public void setDomicilio(String domicilio){ this.domicilio = domicilio; }
	public void setMunicipio(String municipio){ this.municipio = municipio; }
	public void setProvincia(String provincia){ this.provincia = provincia; }
	public void setCodPostal(String codPostal){ this.codPostal = codPostal; }
	public void setTelefono(String telefono){ this.telefono = telefono; }
	public void setEnsenianzas(String ensenianzas){ this.ensenianzas = ensenianzas; }
	public void setServicios(String servicios){ this.servicios = servicios; }
	public void setProgramas(String programas){ this.programas = programas; }
	public void setLatitud(float latitud){ this.latitud = latitud; }
	public void setLongitud(float longitud){ this.longitud = longitud; }
	
	//Getters
	public String getCodigo(){ return codigo; }
	public String getDenominacion(){ return denominacion; }
	public String getNombre(){ return nombre; }
	public String getDependencia(){ return dependencia; }
	public String getDomicilio(){ return domicilio; }
	public String getMunicipio(){ return municipio; }
	public String getProvincia(){ return provincia; }
	public String getCodPostal(){ return codPostal; }
	public String getTelefono(){ return telefono; }
	public String getEnsenianzas(){ return ensenianzas; }
	public String getServicios(){ return servicios; }
	public String getProgramas(){ return programas; }
	public float getLatitud(){ return latitud; }
	public float getLongitud(){ return longitud; }
}