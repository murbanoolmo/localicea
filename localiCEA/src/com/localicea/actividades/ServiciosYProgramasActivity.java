/**
 *  Copyright(c) 2012 by María Urbano Olmo <urbanolmo@gmail.com>
 *
 * localiCEA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * localiCEA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with localiCEA. If not, see <http://www.gnu.org/licenses/>.
 */

package com.localicea.actividades;

import java.util.ArrayList;

import com.localicea.*;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class ServiciosYProgramasActivity extends Activity{
	//botón para volver a la pantalla anterior
	private Button btnBack;
	//botón para volver a la pantalla siguiente
	private Button btnForward;
	//Se crea un ArrayList de tipo Check_Item para la lista de servicios
	ArrayList<Check_Item> lista_servicios = new ArrayList<Check_Item>();
	//Se crea un ArrayList de tipo Check_Item para la lista de programas
	ArrayList<Check_Item> lista_programas = new ArrayList<Check_Item>();		
	//Se crean los objetos tipo ListView
	ListView lstServicios;
	ListView lstProgramas;
	//Se crea un objeto de tipo AdaptadorServiciosYProgramas
	AdaptadorListCheckbox adaptadorServicios;
	AdaptadorListCheckbox adaptadorProgramas;
	//provincia seleccionada en la primera pantalla
	private String provSeleccionada;
	//municipio seleccionado en la segunda pantalla
	private String munSeleccionado;
	//Lista de enseñanzas seleccionadas
	ArrayList<Check_Item> lista_ensenianzas = new ArrayList<Check_Item>();	
	private String filtro_AND_OR;
	
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setWindowAnimations(android.R.anim.slide_in_left);
		setContentView(R.layout.servicios_y_programas);
		
		//extraemos los datos seleccionados en pantallas anteriores
		provSeleccionada = NavigationManager.getProvincia(this);
		munSeleccionado = NavigationManager.getMunicipio(this);
		lista_ensenianzas = NavigationManager.getEnsenianzas(this);
		
		//establecemos el formato de las dos listas que tendremos en esta pantalla
		lstServicios = (ListView) findViewById(R.id.LstServicios);
		lstProgramas = (ListView) findViewById(R.id.LstProgramas);

		if(NavigationManager.getServicios(this) != null)
			lista_servicios = NavigationManager.getServicios(this);
		else{
			//Se añaden nuevos Check_Item en el ArrayList de servicios
			lista_servicios.add(new Check_Item("Actividades extraescolares (AEX)", 0));
			lista_servicios.add(new Check_Item("Aula matinal (AM)", 0));
			lista_servicios.add(new Check_Item("Comedor (COM)", 0));
		}
		
		if(NavigationManager.getProgramas(this) != null)
			lista_programas = NavigationManager.getProgramas(this);
		else{
			//Se añaden nuevos Check_Item en el ArrayList de programas
			lista_programas.add(new Check_Item("Bilingüe Español-Francés (CBEF)", 0));
			lista_programas.add(new Check_Item("Bilingüe Español-Inglés (CBEI)", 0));
			lista_programas.add(new Check_Item("Bilingüe Español-Alemán (CBEA)", 0));
		}	
		
		//Se define un nuevo adaptador de tipo AdaptadorServiciosYProgramas donde se le pasa como argumentos el contexto de la actividad y el arraylist de los dias
		adaptadorServicios = new AdaptadorListCheckbox(this, lista_servicios);
		adaptadorProgramas = new AdaptadorListCheckbox(this, lista_programas);

		//Se establece el adaptador en la Listview
		lstServicios.setAdapter(adaptadorServicios);
		lstProgramas.setAdapter(adaptadorProgramas);

		//Se le aplica un Listener donde ira lo que tiene que hacer en caso de que sea pulsado
		lstServicios.setOnItemClickListener(new OnItemClickListener()
		{
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3)
			{
				//En caso de que la posicion seleccionada gracias a "arg2" sea true que lo cambie a false 
				if (lista_servicios.get(arg2).isChekeado()==1) {
					lista_servicios.get(arg2).setChekeado(0);
				} else {
					//aqui al contrario que la anterior, que lo pase a true.
					lista_servicios.get(arg2).setChekeado(1);
				}
				//Se notifica al adaptador de que el ArrayList que tiene asociado ha sufrido cambios (forzando asi a ir al metodo getView())
				adaptadorServicios.notifyDataSetChanged();
			}
		});
		
		lstProgramas.setOnItemClickListener(new OnItemClickListener() 
		{
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3)
			{
				//En caso de que la posicion seleccionada gracias a "arg2" sea true que lo cambie a false 
				if (lista_programas.get(arg2).isChekeado()==1) {
					lista_programas.get(arg2).setChekeado(0);
				} else {
					//aqui al contrario que la anterior, que lo pase a true.
					lista_programas.get(arg2).setChekeado(1);
				}
				//Se notifica al adaptador de que el ArrayList que tiene asociado ha sufrido cambios (forzando asi a ir al metodo getView())
				adaptadorProgramas.notifyDataSetChanged();
			}
		});
	
		//Le damos formato y acción a los botones de la pantalla
		
		btnBack = (Button) findViewById(R.id.btnVolver);
        //cuando lo pulsemos volvemos a la pantalla de Municipios, 
		//mostrando los correspondientes a la provincia seleccionada anteriormente.
        btnBack.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		NavigationManager.navegarAEnsenianzasActivity(ServiciosYProgramasActivity.this,
        													  provSeleccionada, munSeleccionado,
        													  lista_ensenianzas);
        	};
        });
        
        btnForward = (Button) findViewById(R.id.btnSiguiente);
        //cuando lo pulsemos pasamos a la siguiente pantalla para seleccionar los servicios y programas.
        btnForward.setOnClickListener(new OnClickListener() {
        	
			public void onClick(View v) 
			{        	
				final CharSequence[] items ={"Todos los requisitos",
											  "Alguno de los requisitos"};
				
				AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
								
				builder.setTitle("Buscar centros que cumplan:");
				builder.setItems(items, new DialogInterface.OnClickListener() {
				    public void onClick(DialogInterface dialog, int item) {
				    	filtro_AND_OR = (String) items[item];
				    	dialog.cancel();
				    	
				    	if(filtro_AND_OR.equals("Todos los requisitos")){
				    		NavigationManager.navegarAMapaActivity(ServiciosYProgramasActivity.this,
				    												provSeleccionada, munSeleccionado,
				    												lista_ensenianzas, lista_servicios, 
				    												lista_programas, true);
				    	}else{
				    		NavigationManager.navegarAMapaActivity(ServiciosYProgramasActivity.this,
				    												provSeleccionada, munSeleccionado,
				    												lista_ensenianzas, lista_servicios, 
				    												lista_programas, false);
				    	}
				    }
				});
				AlertDialog alert = builder.create();
				alert.show();
        	};
        });
	}
}