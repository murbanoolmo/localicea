/**
 *  Copyright(c) 2012 by María Urbano Olmo <urbanolmo@gmail.com>
 *
 * localiCEA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * localiCEA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with localiCEA. If not, see <http://www.gnu.org/licenses/>.
 */

package com.localicea.actividades;

import com.localicea.*;
import com.localicea.bd.*;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class MunicipiosActivity extends Activity {
	private DBHelper BD;
	private Button btnBack;
	private String provSeleccionada;
	
	/** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setWindowAnimations(android.R.anim.slide_in_left);
        setContentView(R.layout.municipios);
              
    	//Obtenemos la provincia seleccioanda en la pantalla anterior
    	provSeleccionada = NavigationManager.getProvincia(this);
    	//Creamos y abrimos la base de datos
    	crearBD();
    	//Consultamos la lista de municipios correspondiente a la provincia seleccionada anteriormente
        ArrayList<String> municipios = BD.getMunicipios(provSeleccionada);            
    	//Log.v("",municipios.get(1));
        //Establecemos el formato que van a tener los elementos de la lista de municipios
    	ArrayAdapter<String> adaptador = new ArrayAdapter(this, R.layout.listitem, municipios);
        //Y el formato de la lista en general
        final ListView lstMunicipios = (ListView)findViewById(R.id.LstOpciones);
        //pasamos a la lista, el adaptador con los items concretos de municipios.
        lstMunicipios.setAdapter(adaptador);
        
        //Al pulsar sobre uno de los municipios, pasamos a la siguiente actividad, para seleccionar enseñanzas
        lstMunicipios.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                //Cogemos el municipio seleccionadi para pasarlo junto con la provincia a la siguietne actividad.
            	String munSeleccionado = ((TextView)v).getText().toString();            	
                //navegamos a la actividad Enseñanzas
                NavigationManager.navegarAEnsenianzasActivity(MunicipiosActivity.this, provSeleccionada, munSeleccionado, null);
            }
        }); 
        
        //Creamos un botón para volver a la pantalla anterior
        btnBack = (Button) findViewById(R.id.btnVolver);
        //definimos una acción para el botón cuando lo pulsemos
        btnBack.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		//volvemos a la pantalla principal
        		NavigationManager.navegarAProvinciasActivity(MunicipiosActivity.this);
        	}
       	});        
    }
    
    /**
     * Método para crear la base de datos.
     * */
    public void crearBD() {
    	BD = new DBHelper(this);
        try {
        	BD.createDataBase();
        } catch (IOException ioe) {
            throw new Error("No se puede crear la base de datos.");
        }
    }
}