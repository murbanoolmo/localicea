/**
 *  Copyright(c) 2012 by María Urbano Olmo <urbanolmo@gmail.com>
 *
 * localiCEA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * localiCEA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with localiCEA. If not, see <http://www.gnu.org/licenses/>.
 */

package com.localicea.actividades;

import java.io.IOException;
import java.util.ArrayList;

import com.localicea.*;
import com.localicea.bd.*;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;

public class ProvinciasActivity extends Activity 
{	
	private DBHelper BD;
	
	/** Called when the activity is first created. */
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setWindowAnimations(android.R.anim.slide_in_left);
        setContentView(R.layout.main);
        
       
        //Creamos y abrimos la base de datos
    	crearBD();
    	
    	ArrayList<String> provincias = BD.getProvincias();
    	
    	ArrayAdapter<String> adaptador = new ArrayAdapter(this, R.layout.listitem, provincias);
        
        final ListView lstProvincias = (ListView)findViewById(R.id.LstOpciones);
        
        lstProvincias.setAdapter(adaptador);
        
        lstProvincias.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                //Cogemos la provincia seleccionada para mostrar sus municipios
            	String provSeleccionada = ((TextView)v).getText().toString();            	
            	 
                NavigationManager.navegarAMunicipiosActivity(ProvinciasActivity.this, provSeleccionada);                 
            }
        });       
    }	
       
    public void crearBD() {
    	BD = new DBHelper(this);
        try {
        	BD.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
    }
}