/**
 *  Copyright(c) 2012 by María Urbano Olmo <urbanolmo@gmail.com>
 *
 * localiCEA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * localiCEA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with localiCEA. If not, see <http://www.gnu.org/licenses/>.
 */

package com.localicea.actividades;

import java.util.ArrayList;

import com.localicea.*;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

public class EnsenianzaActivity extends Activity {
	//botón para volver a la pantalla anterior
	private Button btnBack;
	//botón para volver a la pantalla siguiente
	private Button btnForward;
	//Se crea un ArrayList de tipo Check_Item
	ArrayList<Check_Item> lista_ensenianzas = new ArrayList<Check_Item>();
	//Se crea una objeto tipo ListView
	ListView lstLista;
	//Se crea un objeto de tipo AdaptadorServiciosYProgramas
	AdaptadorListCheckbox adaptador;
	//provincia seleccionada en la primera pantalla
	private String provSeleccionada;
	//municipio seleccionado en la segunda pantalla
	private String munSeleccionado;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setWindowAnimations(android.R.anim.slide_in_left);
		setContentView(R.layout.ensenianzas);
		
		provSeleccionada = NavigationManager.getProvincia(this);
		munSeleccionado = NavigationManager.getMunicipio(this);
		
		lstLista = (ListView) findViewById(R.id.LstOpciones);
		
		if(NavigationManager.getEnsenianzas(this) != null)
			lista_ensenianzas = NavigationManager.getEnsenianzas(this);
		else{
			//Se añaden nuevas Check_Item en el ArrayList 
			lista_ensenianzas.add(new Check_Item("Infantil", 0));
			lista_ensenianzas.add(new Check_Item("Primaria", 0));
			lista_ensenianzas.add(new Check_Item("Secundaria", 0));
			lista_ensenianzas.add(new Check_Item("Bachillerato", 0));
			lista_ensenianzas.add(new Check_Item("Ciclo Formativo Grado Medio", 0));
			lista_ensenianzas.add(new Check_Item("Educación Especial", 0));	
			lista_ensenianzas.add(new Check_Item("PCPI", 0));
			lista_ensenianzas.add(new Check_Item("Ciclo Formativo Grado Superior", 0));
			lista_ensenianzas.add(new Check_Item("Adultos", 0));
			lista_ensenianzas.add(new Check_Item("Música", 0));
			lista_ensenianzas.add(new Check_Item("Danza", 0));
			lista_ensenianzas.add(new Check_Item("Arte Dramático", 0));
			lista_ensenianzas.add(new Check_Item("Idiomas", 0));		
			lista_ensenianzas.add(new Check_Item("Artes Plásticas y Diseño", 0));
		}
		
		//Se define un nuevo adaptador de tipo AdaptadorEnsenianza donde se le pasa como argumentos el contexto de la actividad y el arraylist de los dias
		adaptador = new AdaptadorListCheckbox(this, lista_ensenianzas);

		//Se establece el adaptador en la Listview
		lstLista.setAdapter(adaptador);

		//Se le aplica un Listener donde ira lo que tiene que hacer en caso de que sea pulsado
		lstLista.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) {

				//En caso de que la posicion seleccionada gracias a "arg2" sea true que lo cambie a false 
				if (lista_ensenianzas.get(arg2).isChekeado()==1) {
					lista_ensenianzas.get(arg2).setChekeado(0);
				} else {
					//aqui al contrario que la anterior, que lo pase a true.
					lista_ensenianzas.get(arg2).setChekeado(1);
				}
				//Se notifica al adaptador de que el ArrayList que tiene asociado ha sufrido cambios (forzando asi a ir al metodo getView())
				adaptador.notifyDataSetChanged();

			}
		});
	
		//Le damos formato y acción a los botones de la pantalla
		
		btnBack = (Button) findViewById(R.id.btnVolver);
        //cuando lo pulsemos volvemos a la pantalla de Municipios, 
		//mostrando los correspondientes a la provincia seleccionada anteriormente.
        btnBack.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		NavigationManager.navegarAMunicipiosActivity(EnsenianzaActivity.this, provSeleccionada);
        	};
        });
        
        btnForward = (Button) findViewById(R.id.btnSiguiente);
        //cuando lo pulsemos pasamos a la siguiente pantalla para seleccionar los servicios y programas.
        btnForward.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		NavigationManager.navegarAServiciosYProgramasActivity(EnsenianzaActivity.this, provSeleccionada,
        															  munSeleccionado, lista_ensenianzas, null, null);
        	};
        });
	}
}