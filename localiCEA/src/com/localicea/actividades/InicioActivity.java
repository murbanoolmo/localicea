/**
 *  Copyright(c) 2012 by María Urbano Olmo <urbanolmo@gmail.com>
 *
 * localiCEA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * localiCEA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with localiCEA. If not, see <http://www.gnu.org/licenses/>.
 */

package com.localicea.actividades;

import com.localicea.*;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class InicioActivity extends Activity 
{	
	//botón para comenzar a buscar centros
	private Button btnComenzar;
	//botón para ir al tutorial
	private Button btnTutorial;
	/** Called when the activity is first created. */
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        getWindow().setWindowAnimations(android.R.anim.slide_in_left);
        setContentView(R.layout.inicio);
        
        btnComenzar = (Button) findViewById(R.id.btnComenzar);
        //cuando lo pulsemos volvemos a la pantalla de Municipios, 
		//mostrando los correspondientes a la provincia seleccionada anteriormente.
        btnComenzar.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		NavigationManager.navegarAProvinciasActivity(InicioActivity.this);
        	};
        });
        
        btnTutorial = (Button) findViewById(R.id.btnTutorial);
        btnTutorial.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		goToTutorial(v);
        	};
        });
       
    }	
    
    /**
     * Función para dirigirnos a la url del tutorial
     * */
    public void goToTutorial (View view) {
        goToUrl ("https://www.youtube.com/watch?feature=player_embedded&v=nZI3J7zFOCY");
    }
    
    private void goToUrl (String url) {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }
   
}