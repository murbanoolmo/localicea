/**
 *  Copyright(c) 2012 by María Urbano Olmo <urbanolmo@gmail.com>
 *
 * localiCEA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * localiCEA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with localiCEA. If not, see <http://www.gnu.org/licenses/>.
 */

package com.localicea.actividades;

import com.localicea.bd.DBHelper;
import com.localicea.mapa.MapItemizedOverlay;
import com.localicea.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class MapaActivity extends MapActivity
{
	//referencia a nuestra base de datos
	private DBHelper BD;
	//mapa que vamos a pintar
	private MapView map;
	//contralodor para poder, por ejemplo, ajusar el zoom del mapa
	private MapController mapController;
	//variable para guardar nuestro controlador
	private MyLocationOverlay myLocationOverlay;
	//lista de capas para pintar en el mapa
	protected List<Overlay> mapOverlays;
	//provincia seleccionada en la primera pantalla
	private String provSeleccionada;
	//municipio seleccionado en la segunda pantalla
	private String munSeleccionado;
	//Lista de enseñanzas seleccionadas
	ArrayList<Check_Item> lista_ensenianzas = new ArrayList<Check_Item>();
	//Lista de servicios seleccionados
	ArrayList<Check_Item> lista_servicios = new ArrayList<Check_Item>();
	//Lista de programas seleccionados
	ArrayList<Check_Item> lista_programas = new ArrayList<Check_Item>();
	ArrayList<Centro_Item> lista_centros;
	//Se le ha preguntado si quiere buscar los centros que cumplan todos 
	//o alguno de los requisitos que ha seleccionado
	Boolean todosLosRequisitos;
	//Botón para volver a la pantalla anterior
	private Button btnBack;
	//Botón para mostrar mi ubicación
	private ImageButton btnMiUbicacion;
	//Botón para mostrar la lista de centros resultantes de la búsqueda
	private ImageButton btnListaCentros;
	//Pasaremos el teléfono a parte a la capa correspondiente a cada centro
	//para poder realizar la llamada
	private String telefono=""; 
	
	private LocationManager locManager;
	private LocationListener locListener;
				
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	    	
    	super.onCreate(savedInstanceState);
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setWindowAnimations(android.R.anim.slide_in_left);
        setContentView(R.layout.mapa);
        
        //extraemos los datos seleccionados en pantallas anteriores
      	provSeleccionada = NavigationManager.getProvincia(this);
      	munSeleccionado = NavigationManager.getMunicipio(this);
      	lista_ensenianzas = NavigationManager.getEnsenianzas(this);
      	lista_servicios = NavigationManager.getServicios(this);
      	lista_programas = NavigationManager.getProgramas(this);
      	todosLosRequisitos = NavigationManager.getTodosLosRequisitos(this);
                  	
      	//creamos la base de datos
      	crearBD();
        
      	//obtenemos el listado de centros que cumplen los requisitos del usuario
      	lista_centros= this.BD.getCentros(provSeleccionada, munSeleccionado, lista_ensenianzas,
      															 lista_servicios, lista_programas, todosLosRequisitos);
      	
        //Obtenemos una referencia al control MapView
        map = (MapView)findViewById(R.id.mapa);

        //Mostramos los controles de zoom sobre el mapa
        map.setBuiltInZoomControls(true);

        //Creamos nuestra posición en el mapa
        this.crearMyPosition();
        
        //para manejar el mapa y establecer el zoom que debe mostrar
        mapController = map.getController();
               
        // Vamos a obtener una referencia a la lista de capas o overlays del MapView en mapOverlays:
        mapOverlays = map.getOverlays();
        
        // Justo antes del punto donde añadiremos los marcadores vamos a declarar 
        // un objeto drawable para el marcador:
        Drawable drawable = getResources().getDrawable(R.drawable.marker);
        MapItemizedOverlay itemizedoverlay = new MapItemizedOverlay(drawable, map.getContext(), map, MapaActivity.this);
        
        //Si no se han devuelto centros resultantes de la búsqueda, mostramos el siguiente mensaje.
        if(lista_centros.size() == 0)
        {
        	Toast toast1 =
		            Toast.makeText(this.getBaseContext(),
		            		"No hay ningún centro con las características establecidas.", Toast.LENGTH_LONG);			 
    		toast1.show();
        }
        
        // Vamos a añadir los centros
        for(int i = 0; i < lista_centros.size(); i++)
        {      		
      		//Obtenemos la latitud y longitud para situar el centro
      		int latitud = (int) (lista_centros.get(i).getLatitud() * 1E6);
            int longitud = (int) (lista_centros.get(i).getLongitud() * 1E6);
            //Otenemos el título y cuerpo del balloon que vamos a mostrar con los datos del centro
            String titulo = obtenerTituloBalloon(lista_centros.get(i));
            String cuerpo = obtenerCuerpoBalloon(lista_centros.get(i));
            //Creamos una capa con la localización del centro y sus datos para mostrar en el balloon            	
            OverlayItem overlayItem = new OverlayItem(new GeoPoint(latitud,longitud), titulo, cuerpo);             
            itemizedoverlay.addOverlay(overlayItem);  
            
            //pasamos los siguientes datos para poder lanzar acciones desde los botones del balloon
            itemizedoverlay.setTelefono(telefono);
            itemizedoverlay.setMyLocation(myLocationOverlay);
            itemizedoverlay.setCentroLocation(new GeoPoint(latitud,longitud));            
        }
        //colocamos en el mapa todas las capas con los centros a mostrar                 
        mapOverlays.add(itemizedoverlay);        
        
        //centramos el mapa y establecemos el zoom adecuado para que todos los centros se vean
        zoom_y_centrarMapa(lista_centros);
        
        if(this.hasInternet(this) == false)
        {
        	Toast toast1 =
		            Toast.makeText(this.getBaseContext(),
		            		"Sin conexión a internet no es posible visualizar los mapas.", Toast.LENGTH_LONG);			 
    		toast1.show();
        }
        
        btnBack = (Button) findViewById(R.id.btnVolver);
        //cuando lo pulsemos volvemos a la pantalla de Municipios, 
		//mostrando los correspondientes a la provincia seleccionada anteriormente.
        btnBack.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		NavigationManager.navegarAServiciosYProgramasActivity(MapaActivity.this, provSeleccionada, munSeleccionado, 
        															  lista_ensenianzas, lista_servicios, lista_programas);
        	};
        });  
        
        btnMiUbicacion = (ImageButton) findViewById(R.id.btnMiUbicacion);
        btnMiUbicacion.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		//centramos el mapa en nuestra posición
                centerMyPosition();
        	};
        }); 
        
        btnListaCentros = (ImageButton) findViewById(R.id.btnListaCentros);
        btnListaCentros.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		final String[] items = new String[lista_centros.size()];
        		
        		// Vamos a añadir los centros
                for(int i = 0; i < lista_centros.size(); i++)
                {                	
                	items[i]=obtenerTituloBalloon(lista_centros.get(i));
                }
               
        		AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
	
        		builder.setTitle(lista_centros.size()+" centros mostrados:");
        		
        		builder.setItems(items, new DialogInterface.OnClickListener() {
        			public void onClick(DialogInterface dialog, int item)
        			{        				
        				//Obtenemos la latitud y longitud para situar el centro
        	      		int latitud = (int) (lista_centros.get(item).getLatitud() * 1E6);
        	            int longitud = (int) (lista_centros.get(item).getLongitud() * 1E6);
        	            mapController.setZoom(18);  
        				mapController.animateTo(new GeoPoint(latitud,longitud));
        				dialog.cancel();
        			 }
				});
				AlertDialog alert = builder.create();
				alert.show();
        	};
        }); 
        		
    }
    
    /**
     * Función para centrar el mapa y establecer el zoom de modo que todos los 
     * centros queden visibles.
     * */
    private void zoom_y_centrarMapa(ArrayList<Centro_Item> centros) 
    {
    	int minLat = Integer.MAX_VALUE;
    	int maxLat = Integer.MIN_VALUE;
    	int minLon = Integer.MAX_VALUE;
    	int maxLon = Integer.MIN_VALUE;

    	for (int i = 0; i < centros.size(); i++) 
    	{ 

    	      int lat = (int) (centros.get(i).getLatitud() * 1E6);
    	      int lon = (int) (centros.get(i).getLongitud() * 1E6);

    	      maxLat = Math.max(lat, maxLat);
    	      minLat = Math.min(lat, minLat);
    	      maxLon = Math.max(lon, maxLon);
    	      minLon = Math.min(lon, minLon);
    	 }

    	mapController.zoomToSpan(Math.abs(maxLat - minLat), Math.abs(maxLon - minLon));
    	mapController.animateTo(new GeoPoint( (maxLat + minLat)/2, 
    	(maxLon + minLon)/2 )); 		
	}

	/**
     * Formamos el cuerpo del Ballon con todos los datos del centro 
     */
    private String obtenerCuerpoBalloon(Centro_Item centro) 
    {
    	String cuerpo = "";
    	
    	if(centro.getDependencia() != null){
    		cuerpo = new String("Dependencia: " + centro.getDependencia());
    	}
    	
    	if(centro.getDomicilio() != null){
    		if(cuerpo == "")
    			cuerpo = new String("Domicilio: " + centro.getDomicilio());
    		else cuerpo = cuerpo.concat("\nDomicilio: " + centro.getDomicilio());
    	}
    	
    	if(centro.getEnsenianzas() != null){
    		if(cuerpo == "")
    			cuerpo = new String("Enseñanzas: " + centro.getEnsenianzas());
    		else cuerpo = cuerpo.concat("\nEnseñanzas: " + centro.getEnsenianzas());
    	}
    	
    	if(centro.getServicios() != null){
    		if(cuerpo == "")
    			cuerpo = new String("Servicios: " + centro.getServicios());
    		else cuerpo = cuerpo.concat("\nServicios: " + centro.getServicios());
    	}
    	
    	if(centro.getProgramas() != null){
    		if(cuerpo == "")
    			cuerpo = new String("Programas: " + centro.getProgramas());
    		else cuerpo = cuerpo.concat("\nProgramas: " + centro.getProgramas());
    	}
    	
    	if(centro.getTelefono() != null){
    		if(cuerpo == "")
    			cuerpo = new String("Teléfono: " + centro.getTelefono());
    		else cuerpo = cuerpo.concat("\nTeléfono: " + centro.getTelefono());
    		//sacamos además el teléfono para pasarlo a parte al balloon para poder realizar la llamada.
    		telefono = centro.getTelefono();
    	}
    	
		return cuerpo;
	}

	/**
     * Formamos el titulo del Ballon con la denominación del centro y su nombre 
     */
    private String obtenerTituloBalloon(Centro_Item centro) 
    {		
    	String titulo = "";
    	
    	if(centro.getDenominacion() != null)
        	titulo = new String(centro.getDenominacion());
    	
        if(centro.getNombre() != null)
        	if(titulo != "")
        		titulo = titulo.concat(" "+centro.getNombre());
        	else titulo = new String(centro.getNombre());
		
        return titulo;
	}

    /**
     * Creamos nuestra posición en el mapa 
     */
    private void crearMyPosition()
    {  
    	// Inicializamos el objeto myLocationOverlay pasándole el Context actual y el mapview con el que estamos trabajando.
        myLocationOverlay = new MyLocationOverlay(this, map);
    
        // Añadimos al mapview la nueva capa que hemos creado
        map.getOverlays().add(myLocationOverlay);

        // Habilitamos la brújula
        myLocationOverlay.enableCompass();
              
        // Habilitamos nuestra posición (cogerá la última conocida hasta que localice el dispositivo).
        myLocationOverlay.enableMyLocation();   
    }
    
	/**
     * Centramos el mapa en nuestra posición 
     */
    private void centerMyPosition()
    {   	
    	if(myLocationOverlay.getMyLocation() == null)
    	{
    		Toast toast1 =
		            Toast.makeText(this.getBaseContext(),
		            		"Cargando localización." +
		            	    "\nEspere unos segundos.", Toast.LENGTH_LONG);			 
    		toast1.show();
    	}
    	// Seteamos el método runOnFirstFix para que en cuanto se situe muestre nuestra posición en el mapview.
        myLocationOverlay.runOnFirstFix(new Runnable() {
            public void run() {          
            	mapController.setZoom(16);
                mapController.animateTo(myLocationOverlay.getMyLocation());
            }
        });
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
	
	/**
     * Método para crear la base de datos.
     * */
    public void crearBD() {
    	BD = new DBHelper(this);
        try {
        	BD.createDataBase();
        } catch (IOException ioe) {
            throw new Error("No se puede crear la base de datos.");
        }
    }   
    
    /**
     * Función para comprobar si se dispone de conexión a internet
     * */
    public static boolean hasInternet(Activity a)
    {
    	boolean hasConnectedWifi = false;
    	boolean hasConnectedMobile = false;

    	ConnectivityManager cm = (ConnectivityManager) a.getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo[] netInfo = cm.getAllNetworkInfo();
    	for (NetworkInfo ni : netInfo) 
    	{
    		if (ni.getTypeName().equalsIgnoreCase("wifi"))
    			if (ni.isConnected())
    				hasConnectedWifi = true;
    		if (ni.getTypeName().equalsIgnoreCase("mobile"))
    			if (ni.isConnected())
    				hasConnectedMobile = true;
    	}
    	return hasConnectedWifi || hasConnectedMobile;
    }
}
