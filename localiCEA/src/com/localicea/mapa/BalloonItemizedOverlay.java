/**
 *  Copyright(c) 2012 by María Urbano Olmo <urbanolmo@gmail.com>
 *
 * localiCEA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * localiCEA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with localiCEA. If not, see <http://www.gnu.org/licenses/>.
 */

package com.localicea.mapa;

import com.localicea.*;
import java.util.*;

import android.app.ActionBar.*;
import android.content.*;
import android.graphics.drawable.*;
import android.util.Log;
import android.view.*;
import android.view.View.*;

import com.google.android.maps.*;

public abstract class BalloonItemizedOverlay<Item extends OverlayItem> extends ItemizedOverlay<Item> {

	 private MapView mapView;
	 private BalloonOverlayView<Item> balloonView;
	 private View clickRegion;
	 private int viewOffset;
	 final MapController mc;
	 private Item currentFocussedItem;
	 private int currentFocussedIndex;
	 private Context mContext;
	 //las siguietnes variables las utilizamos para recibir información desde la actividad MapaActivity
	 private MapActivity actividad;
	 private ArrayList<String> telefono = new ArrayList<String>();
	 private MyLocationOverlay myLocationOverlay;
	 private ArrayList<GeoPoint> centroLoc = new ArrayList<GeoPoint>();
		 
	 public BalloonItemizedOverlay(Drawable defaultMarker, MapView mapView, MapActivity mapaActivity)
	 {
		 super(defaultMarker);
		 mContext = mapView.getContext();
		 this.mapView = mapView;
		 viewOffset = 0;
		 mc = mapView.getController();
		 actividad = mapaActivity;
	 }
	 
	 public void setBalloonBottomOffset(int pixels) {
		 viewOffset = pixels;
	 }
	 
	 public int getBalloonBottomOffset() {
		 return viewOffset;
	 }
	 
	 protected boolean onBalloonTap(int index, Item item) {
		 return false;
	 }
	 
	 @Override
	 /**
	  * Se encarga de detectar cual es el marcador que se ha pulsado, comprobar si ya tenemos otros balloon
	  * abiertos y cerrarlos, obtener el nuevo balloon, pasarle los datos y mostrarlo.
	  */
	 protected final boolean onTap(int index)
	 {
		 currentFocussedIndex = index;
		 currentFocussedItem = createItem(index);
	  
		 boolean isRecycled;
		 if (balloonView == null) {
			 balloonView = createBalloonOverlayView();
			 clickRegion = (View) balloonView.findViewById(R.id.balloon_inner_layout);
			 clickRegion.setOnTouchListener(createBalloonTouchListener());
			 isRecycled = false;
		 } else {
			 isRecycled = true;
		 }
	 
		 balloonView.setVisibility(View.GONE);
	  
		 List<Overlay> mapOverlays = mapView.getOverlays();
		 if (mapOverlays.size() > 1) {
			 hideOtherBalloons(mapOverlays);
		 }
	  
		 balloonView.setData(currentFocussedItem, mContext, this.telefono.get(index),
				 			 this.myLocationOverlay, this.centroLoc.get(index));
	  
		 GeoPoint point = currentFocussedItem.getPoint();
	  	 MapView.LayoutParams params = new MapView.LayoutParams(
	  			 LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, point,
	  			 MapView.LayoutParams.BOTTOM_CENTER);
	  	 params.mode = MapView.LayoutParams.MODE_MAP;
	  
	  	 balloonView.setVisibility(View.VISIBLE);
	    
	  	 if (isRecycled) {
	  		 balloonView.setLayoutParams(params);
	  	 } else {
	  		 mapView.addView(balloonView, params);
	  	 }
	  
	  	 mc.animateTo(point);
	  
	  	 return true;
	 }
	 
	 protected BalloonOverlayView<Item> createBalloonOverlayView() {
		 return new BalloonOverlayView<Item>(getMapView().getContext(), getBalloonBottomOffset(),
				 this.actividad);
	 }
	 
	 protected MapView getMapView() {
		 return mapView;
	 }
	 
	 protected void hideBalloon() {
		 if (balloonView != null) {
			 balloonView.setVisibility(View.GONE);
		 }
	 }
	 
	 private void hideOtherBalloons(List<Overlay> overlays) {  
		 for (Overlay overlay : overlays) {
			 if (overlay instanceof BalloonItemizedOverlay<?> && overlay != this) {
				 ((BalloonItemizedOverlay<?>) overlay).hideBalloon();
			 }
		 }  
	 }
	 
	 /**
	  * Setea el método onTouchListener, verifica cuando empezamos a presionar 
	  * con el dedo la pantalla (ACTION_DOWN) y cuando dejamos de presionar (ACTION_UP),
	  * en ese momento llamamos al método onBalloonTap de nuestro balloon.
	  * @return
	  */
	 private OnTouchListener createBalloonTouchListener()
	 {
		 return new OnTouchListener() {
			 public boolean onTouch(View v, MotionEvent event) {
	    
				 View l =  ((View) v.getParent()).findViewById(R.id.balloon_main_layout);
				 Drawable d = l.getBackground();
	    
				 if (event.getAction() == MotionEvent.ACTION_DOWN) {
					 int[] states = {android.R.attr.state_pressed};
					 if (d.setState(states)) {
						 d.invalidateSelf();
					 }
					 return true;
				 } else if (event.getAction() == MotionEvent.ACTION_UP) {
					 int newStates[] = {};
					 if (d.setState(newStates)) {
						 d.invalidateSelf();
					 }
					 onBalloonTap(currentFocussedIndex, currentFocussedItem);
					 return true;
				 } else {
					 return false;
				 }
	    
			 }
		 };
	 }	 
	 
	 //funciones para recibir los datos de la actividad MapaActivity,
	 //necesarios para la funcionalidad de los botones
	 public void setTelefono(String tlf){
		 this.telefono.add(tlf);
	 }
	 
	 public void setMyLocation(MyLocationOverlay myLoc){
		this.myLocationOverlay = myLoc;
	 }
	 
	 public void setCentroLocation(GeoPoint centroLoc)
	 {
		 this.centroLoc.add(centroLoc);
	 }
		 
}