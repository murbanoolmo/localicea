/**
 *  Copyright(c) 2012 by María Urbano Olmo <urbanolmo@gmail.com>
 *
 * localiCEA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * localiCEA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with localiCEA. If not, see <http://www.gnu.org/licenses/>.
 */

package com.localicea.mapa;

import java.util.*;

import android.content.*;
import android.graphics.drawable.*;

import com.google.android.maps.*;
import com.localicea.actividades.MapaActivity;

public class MapItemizedOverlay extends BalloonItemizedOverlay<OverlayItem> {

	 private ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();
		 
	 public MapItemizedOverlay(Drawable defaultMarker, Context context, MapView mapview, MapaActivity mapaActivity) {
	    super(boundCenter(defaultMarker),mapview,mapaActivity);    
	 }
	 
	 public void addOverlay(OverlayItem overlay) {
	     mOverlays.add(overlay);
	     populate();     
	 }
	 
	 @Override
	 protected OverlayItem createItem(int i) {
	   return mOverlays.get(i);
	 }
	  
	 @Override
	 protected boolean onBalloonTap(int index, OverlayItem item) {
	  return true;
	 }
	 
	 @Override
	 public int size() {
	   return mOverlays.size();
	 }
	
}