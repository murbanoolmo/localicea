
/**
 *  Copyright(c) 2012 by María Urbano Olmo <urbanolmo@gmail.com>
 *
 * localiCEA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * localiCEA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with localiCEA. If not, see <http://www.gnu.org/licenses/>.
 */
package com.localicea.mapa;

import java.io.IOException;
import java.util.ArrayList;

import com.localicea.*;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.*;
import android.net.Uri;
import android.util.Log;
import android.view.*;
import android.widget.*;

import com.google.android.maps.*;

@SuppressLint("NewApi")
public class BalloonOverlayView<Item extends OverlayItem> extends FrameLayout {

	 protected LinearLayout layout;
	 protected TextView title;
	 protected TextView snippet;
	 protected LayoutInflater layoutinflater;
	 protected View balloonview;
	 protected ImageView imgclose;
	 //Botón para realizar una llamada
	 private ImageButton btnCall;
	 //Botón para mostrar la ruta desde mi ubicación hasta el centro seleccionado
	 private ImageButton btnRoute;
	 public Intent callIntent;
	 //tanto la actividad origen (MapaActivity) como el teléfono del centro 
	 //se pasan por las clases que van a representar el balloon para poder realizar la llamada.
	 private MapActivity actividad;
	 private String telefono = "";
	 private MyLocationOverlay myLocationOverlay;
	 private GeoPoint centroLoc;
	
	 /**
	  * Este método nos va a servir para obtener la referencia al layout del balloon
	  * @return
	  */
	 protected int getIdView(){return R.layout.window_balloon_overlay;}	 
	 
	 public BalloonOverlayView(final Context context, int balloonBottomOffset, MapActivity mapaActivity)
	 {
		 super(context);
	  
		 actividad = mapaActivity;
		 setPadding(20, 0, 10, balloonBottomOffset);
		 layout = new LinearLayout(context);
		 layout.setVisibility(VISIBLE);
		 layoutinflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 balloonview = layoutinflater.inflate(getIdView(), layout);
		 title = (TextView) balloonview.findViewById(R.id.balloon_item_title);
		 snippet = (TextView) balloonview.findViewById(R.id.balloon_item_snippet);
		 
		 title.setVisibility(GONE);
		 snippet.setVisibility(GONE);
		 
		 btnCall = (ImageButton) balloonview.findViewById(R.id.btnPhone);
	     btnCall.setOnClickListener(new OnClickListener() {
	        	public void onClick(View v) {
	        		llamarAlCentro();	        		
	        	};
	     }); 
	     
	     btnRoute = (ImageButton) balloonview.findViewById(R.id.btnRoute);
	     btnRoute.setOnClickListener(new OnClickListener() {
	        	public void onClick(View v) {
	        		try {
						mostrarRuta(v);
					} catch (IOException e) {
						Log.e("Ruta", "Fallo al calcular la ruta");
					}
	        	};
	     }); 

		 imgclose = (ImageView) balloonview.findViewById(R.id.close_img_button);
		 imgclose.setOnClickListener(new OnClickListener() {
			 public void onClick(View v) {layout.setVisibility(GONE);}
		 });
	  				 
		 FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		 params.gravity = Gravity.NO_GRAVITY;
		 addView(layout, params);   
	 }
	 
	 /**
	  * Sirve para hacer visible nuestro balloon y rellenar los TextView desde un elemento Item.
	  * @param item
	  * @param mContext
	  * @param myLoc 
	  * @param centroLoc 
	  */
	 public void setData(Item item, Context mContext, String tlf, MyLocationOverlay myLoc, GeoPoint centroLoc) 
	 {
		  //almacenamos las variables que se han ido pasando desde la actividad mapa, para poder realizar las 
		  //acciones de los botones.
		  this.telefono = tlf;
		  this.myLocationOverlay = myLoc;
		  this.centroLoc = centroLoc;
		  
	 	  layout.setVisibility(VISIBLE);
	 	  if (item.getTitle() != null && item.getTitle().length() > 0) {
	 		  title.setVisibility(VISIBLE);
	 		  title.setText(item.getTitle());
	 	  } else {
	 		  title.setVisibility(GONE);
	 	  }
	 	  if (item.getSnippet() != null && item.getSnippet().length() > 0) {
	 		  snippet.setVisibility(VISIBLE);
	 		  snippet.setText(item.getSnippet());
	 	  } else {
	 		  snippet.setVisibility(GONE);
	 	  }  
	 }
	 
	 public void llamarAlCentro() {
		    try {
		    	if(telefono == null) telefono="";
		        callIntent = new Intent(Intent.ACTION_CALL);
		        callIntent.setData(Uri.parse("tel:"+telefono));		       
		        actividad.startActivity(callIntent);
		    } catch (ActivityNotFoundException activityException) {
		        Log.e("Marcando", "Fallo en la llamada.", activityException);
		    }
	 }
	 
	 public void mostrarRuta(View v) throws IOException
	 {
		 if(this.myLocationOverlay.getMyLocation() != null)
		 {
			 double latitudOrigen = this.myLocationOverlay.getMyLocation().getLatitudeE6()/1E6;
			 double longitudOrigen = this.myLocationOverlay.getMyLocation().getLongitudeE6()/1E6;
			 double latitudDestino = this.centroLoc.getLatitudeE6()/1E6;
			 double longitudDestino = this.centroLoc.getLongitudeE6()/1E6;
		 
			 final Intent intent = new Intent(Intent.ACTION_VIEW,
				 Uri.parse("http://maps.google.com/maps?" + "saddr="+ latitudOrigen + "," + longitudOrigen + 
						 									"&daddr=" + latitudDestino + "," + longitudDestino));
			 intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
 	                        actividad.startActivity(intent);
		 }
		 else{
			 Toast toast1 =
			            Toast.makeText(v.getContext(),
			            		"Cargando localización." +
					            "\nEspere unos segundos.", Toast.LENGTH_LONG);			 
			 toast1.show();
		 }
		
	 }
}