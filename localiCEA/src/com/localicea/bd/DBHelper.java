/**
 *  Copyright(c) 2012 by María Urbano Olmo <urbanolmo@gmail.com>
 *
 * localiCEA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * localiCEA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with localiCEA. If not, see <http://www.gnu.org/licenses/>.
 */

package com.localicea.bd;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import com.localicea.*;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

	// Ruta por defecto de las bases de datos en el sistema Android
	private static String DB_PATH = "/data/data/com.localicea/databases/";

	private static String DB_NAME = "localicea_bd.sqlite";

	private static int DATABASE_VERSION = 1;
		 
	private SQLiteDatabase db;

	private final Context myContext;

	/**
	 * Constructor: Toma referencia hacia el contexto de la aplicación que lo
	 * invoca para poder acceder a los 'assets' y 'resources' de la aplicación.
	 * Crea un objeto DBOpenHelper que nos permitirá controlar la apertura de la
	 * base de datos.
	 * 
	 * @param context
	 */
	public DBHelper(Context context) {
		super(context, DB_NAME, null, DATABASE_VERSION);
		this.myContext = context;
	}

	/**
	 * Crea una base de datos vacía en el sistema y la reescribe con nuestro
	 * fichero de base de datos.
	 * */
	public void createDataBase() throws IOException 
	{
		boolean dbExist = checkDataBase();

		if (dbExist) {
			// la base de datos existe y no hacemos nada.
		} else {
			// Llamando a este método se crea la base de datos vacía en la ruta
			// por defecto del sistema de nuestra aplicación
			// por lo que podremos sobreescribirla con
			// nuestra base de datos.
			this.getReadableDatabase();

			try {

				copyDataBase();

			} catch (IOException e) {
				throw new Error("Error copiando Base de Datos");
			}
		}

	}

	/**
	 * Comprueba si la base de datos existe para evitar copiar siempre el
	 * fichero cada vez que se abra la aplicación.
	 * 
	 * @return true si existe, false si no existe
	 */
	private boolean checkDataBase()
	{
		SQLiteDatabase checkDB = null;

		try {

			String myPath = DB_PATH + DB_NAME;
			checkDB = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READWRITE);

		} catch (SQLiteException e) {

			// si llegamos aqui es porque la base de datos no existe todavía.

		}
		if (checkDB != null) {

			checkDB.close();

		}
		return checkDB != null ? true : false;
	}

	/**
	 * Copia nuestra base de datos desde la carpeta assets a la recién creada
	 * base de datos en la carpeta de sistema, desde dónde podremos acceder a
	 * ella. Esto se hace con bytestream.
	 * */
	private void copyDataBase() throws IOException {

		// Abrimos el fichero de base de datos como entrada
		InputStream myInput = myContext.getAssets().open(DB_NAME);
		
		// Ruta a la base de datos vacía recién creada
		String outFileName = DB_PATH + DB_NAME;

		// Abrimos la base de datos vacía como salida
		OutputStream myOutput = new FileOutputStream(outFileName);

		// Transferimos los bytes desde el fichero de entrada al de salida
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}
	
		// Liberamos los streams
		myOutput.flush();
		myOutput.close();
		myInput.close();
	}
	
	/**
     * Abre la base de datos
     **/
	 public void openBD() throws SQLException{
         String myPath = DB_PATH + DB_NAME;
         db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

     }
	 
	/**
     * Cierra la base de datos
     **/
    public synchronized void closeDB() {
            if(db != null)
                db.close();

            super.close();
    }
    
	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}
	
	/**
	 * @return Devuelve un listado con las distintas provincias de la comunidad autónoma
	 * */
	public ArrayList<String> getProvincias() 
	{
		final String TABLE_NAME = "provincias";
		final String TABLE_KEY_ROWID = "rowid";
		final String TABLE_KEY_PROVINCIA = "Provincia";
		
		ArrayList<String> lstProvincias = new ArrayList<String>();
		try{
			this.openBD();
			
			Cursor result = db.query(TABLE_NAME, new String[] {TABLE_KEY_ROWID,TABLE_KEY_PROVINCIA}, null, null, null, null, null);
		
			if(result.moveToFirst()){
	            do{
	            	lstProvincias.add(result.getString(1));
	            }while(result.moveToNext());
	            
	            if(result!=null && !result.isClosed()){
	                result.close();
	            }
	        }
			
			this.closeDB();
		}catch (Exception e)
		{
			Log.e("ERROR", "ERROR IN CODE:"+e);
		}
		
		return lstProvincias;
	}
	
	/**
	 * @return Devuelve el listado de municipios correspondiente a la provincia pasada por parámetro
	 * */
	public ArrayList<String> getMunicipios(String provincia) 
	{
		final String TABLE_NAME = "municipios";
		final String TABLE_KEY_ROWID = "rowid";
		final String TABLE_KEY_PROVINCIA = "Provincia";
		final String TABLE_KEY_MUNICIPIO = "Municipio";
		
		ArrayList<String> lstMunicipios = new ArrayList<String>();
		try{
			this.openBD();
			
			Cursor result = db.query(TABLE_NAME, new String[] {TABLE_KEY_ROWID,TABLE_KEY_PROVINCIA,TABLE_KEY_MUNICIPIO}, TABLE_KEY_PROVINCIA + " LIKE '" + provincia+"'", null, null, null, null);
		
			if(result.moveToFirst()){
	            do{
	            	lstMunicipios.add(result.getString(2));
	            }while(result.moveToNext());
	            
	            if(result!=null && !result.isClosed()){
	                result.close();
	            }
	        }
			
			this.closeDB();
		}catch (Exception e)
		{
			Log.e("ERROR", "ERROR IN CODE:"+e);
		}
		
		return lstMunicipios;
	}
	
	/**
	 * @return Devuelve un listado con los centros que cumplan los requisitos establecidos por el usuario
	 * */
	public ArrayList<Centro_Item> getCentros(String provincia, String municipio,
											  ArrayList<Check_Item> lista_ensenianzas,
											  ArrayList<Check_Item> lista_servicios,  
											  ArrayList<Check_Item> lista_programas,
											  Boolean todosLosRequisitos) 
	{
		final String TABLE_NAME = "centros";
		final String TABLE_KEY_PROVINCIA = "Provincia";
		final String TABLE_KEY_MUNICIPIO = "Municipio";
		final String TABLE_KEY_ROWID = "rowid";
		final String TABLE_KEY_CODIGO = "Código";
		final String TABLE_KEY_DENOMINA = "Denominación";		
		final String TABLE_KEY_NOMBRE = "Nombre";
		final String TABLE_KEY_DEPENDENCIA = "Dependencia";
		final String TABLE_KEY_DOMICILIO = "Domicilio";
		final String TABLE_KEY_CODPOSTAL = "CódPostal";
		final String TABLE_KEY_TELEFONO = "Teléfono";
		final String TABLE_KEY_ENSENIANZAS = "Enseñanzas";
		final String TABLE_KEY_SERVICIOS = "Servicios";
		final String TABLE_KEY_PROGRAMAS = "Programas";
		final String TABLE_KEY_LATITUD = "lat";
		final String TABLE_KEY_LONGITUD = "long";
		String ensenianzas = "";
		String servicios = "";
		String programas = "";
		
		//Conjunto de columnas que queremos que nos devuelva la consulta a la tabla centros
		String[] proyeccion = new String[] {TABLE_KEY_ROWID, TABLE_KEY_CODIGO,
											TABLE_KEY_DENOMINA, TABLE_KEY_NOMBRE,
											TABLE_KEY_DEPENDENCIA, TABLE_KEY_DOMICILIO,
											TABLE_KEY_CODPOSTAL, TABLE_KEY_TELEFONO,
											TABLE_KEY_ENSENIANZAS, TABLE_KEY_SERVICIOS,TABLE_KEY_PROGRAMAS,											
											TABLE_KEY_LATITUD, TABLE_KEY_LONGITUD};
		
		//Array para almacenar el listado de los centros devueltos y que cumplen las condiciones establecidas por el usuario 
		ArrayList<Centro_Item> lstCentros = new ArrayList<Centro_Item>();
		
		//Obtenemos las cláusulas WHERE para las enseñanzas, servicios y programas seleccionados por el ususario
		//Tendremos en cuenta si el usuario quiere que busquemos centros que cumplan TODOS los requisitos que han marcado
		//o tan sólo ALGUNO de los requisitos.
		if(todosLosRequisitos == true){
			ensenianzas = this.obtenerCadenaEnsenianzasAND(TABLE_KEY_ENSENIANZAS, lista_ensenianzas);
			servicios = this.obtenerCadenaServiciosAND(TABLE_KEY_SERVICIOS, lista_servicios);	
			programas = this.obtenerCadenaProgramasAND(TABLE_KEY_PROGRAMAS, lista_programas);	
		}else{
			ensenianzas = this.obtenerCadenaEnsenianzasOR(TABLE_KEY_ENSENIANZAS, lista_ensenianzas);
			servicios = this.obtenerCadenaServiciosOR(TABLE_KEY_SERVICIOS, lista_servicios);
			programas = this.obtenerCadenaProgramasOR(TABLE_KEY_PROGRAMAS, lista_programas);	
		}
			
		//Si no se han seleccionado enseñanzas, haremos una condición para que no filtre ninguna
		if(ensenianzas == "")
			ensenianzas = " LIKE \"%\" OR "+ TABLE_KEY_ENSENIANZAS + " is null";
				
		//Si no se han seleccionado servicios, haremos una condición para que no filtre ninguno
		if(servicios == "")
			servicios = " LIKE \"%\" OR "+ TABLE_KEY_SERVICIOS+ " is null";
				
		//Si no se han seleccionado programas, haremos una condición para que no filtre ninguno
		if(programas == "")
			programas = " LIKE \"%\" OR "+ TABLE_KEY_PROGRAMAS+ " is null";
				
		//lanzamos la consulta que nos devolverá los centros que cumplen los requisitos del usuario
		try{
			this.openBD();
			
			Cursor result = db.query(TABLE_NAME, proyeccion, 
							TABLE_KEY_PROVINCIA + " LIKE '" + provincia + "' AND " +
							TABLE_KEY_MUNICIPIO + " LIKE '" + municipio + "' AND (" +
							TABLE_KEY_ENSENIANZAS + ensenianzas + ") AND (" +
							TABLE_KEY_SERVICIOS + servicios + ") AND (" +
							TABLE_KEY_PROGRAMAS + programas + ")", 
							null, null, null, null);		
		
			//formamos la lista de centros, recorriendo el cursor y añadiendo un centro en cada iteración
			if(result.moveToFirst()){
	            do{
	            	Centro_Item c = new Centro_Item(
	            			result.getString(1),
	            			result.getString(2),
	            			result.getString(3),
	            			result.getString(4),
	            			result.getString(5),
	            			municipio,
	            			provincia,
	            			result.getString(6),
	            			result.getString(7),
	            			result.getString(8),
	            			result.getString(9),
	            			result.getString(10),
	            			result.getString(11),
	            			result.getString(12)
	            			);
	            	lstCentros.add(c);
	            }while(result.moveToNext());
	            
	            if(result!=null && !result.isClosed()){
	                result.close();
	            }
	        }
			
			this.closeDB();
		}catch (Exception e)
		{
			Log.e("ERROR", "ERROR IN CODE:"+e);
		}
		
		return lstCentros;
	}
	
	/**
	 * @return Devuelve la condición para filtrar los centros que tengan TODOS los servicios seleccionados
	 * */
	public String obtenerCadenaServiciosAND (String nomTablaServicios, ArrayList<Check_Item> lista_servicios)
	{
		String servicios="";
		
		for (int i = 0; i < lista_servicios.size(); i++)
		{
			switch(i)
			{
				case 0: 
					if(lista_servicios.get(i).isChekeado()==1)
						servicios = new String(" LIKE \"%AEX%\"");
					break;
				case 1:
					if(lista_servicios.get(i).isChekeado()==1)
					{
						if(servicios == "")
							servicios = new String(" LIKE \"%AM%\"");
						else servicios = servicios.concat(" AND " + nomTablaServicios +" LIKE \"%AM%\"");
					}
					break;
				case 2:
					if(lista_servicios.get(i).isChekeado()==1)
					{
						if(servicios == "")
							servicios = new String(" LIKE \"%COM%\"");
						else servicios = servicios.concat(" AND " + nomTablaServicios +" LIKE \"%COM%\"");
					}
			}//fin switch				
		}//fin for
		
		return servicios;
	}
	
	/**
	 * @return Devuelve la condición para filtrar los centros que tengan ALGUNO los servicios seleccionados
	 * */
	public String obtenerCadenaServiciosOR(String nomTablaServicios, ArrayList<Check_Item> lista_servicios)
	{
		String servicios="";
		
		for (int i = 0; i < lista_servicios.size(); i++)
		{
			switch(i)
			{
				case 0: 
					if(lista_servicios.get(i).isChekeado()==1)
						servicios = new String(" LIKE \"%AEX%\"");
					break;
				case 1:
					if(lista_servicios.get(i).isChekeado()==1)
					{
						if(servicios == "")
							servicios = new String(" LIKE \"%AM%\"");
						else servicios = servicios.concat(" OR " + nomTablaServicios +" LIKE \"%AM%\"");
					}
					break;
				case 2:
					if(lista_servicios.get(i).isChekeado()==1)
					{
						if(servicios == "")
							servicios = new String(" LIKE \"%COM%\"");
						else servicios = servicios.concat(" OR " + nomTablaServicios +" LIKE \"%COM%\"");
					}
			}//fin switch				
		}//fin for
		
		return servicios;
	}
	
	/**
	 * @return Devuelve la condición para filtrar los centros que tengan TODOS los programas seleccionados
	 * */
	public String obtenerCadenaProgramasAND(String nomTablaProgramas, ArrayList<Check_Item> lista_programas)
	{
		String programas="";
		
		for (int i = 0; i < lista_programas.size(); i++)
		{
			switch(i)
			{
				case 0: 
					if(lista_programas.get(i).isChekeado()==1)
						programas = new String(" LIKE \"%CBEF%\"");
					break;
				case 1:
					if(lista_programas.get(i).isChekeado()==1)
					{
						if(programas == "")
							programas = new String(" LIKE \"%CBEI%\"");
						else programas = programas.concat(" AND " + nomTablaProgramas +" LIKE \"%CBEI%\"");
					}
					break;
				case 2:
					if(lista_programas.get(i).isChekeado()==1)
					{
						if(programas == "")
							programas = new String(" LIKE \"%CBEA%\"");
						else programas = programas.concat(" AND " + nomTablaProgramas +" LIKE \"%CBEA%\"");
					}
			}//fin switch				
		}//fin for
		
		return programas;
	}
	
	/**
	 * @return Devuelve la condición para filtrar los centros que tengan ALGUNOS de los programas seleccionados
	 * */
	public String obtenerCadenaProgramasOR(String nomTablaProgramas, ArrayList<Check_Item> lista_programas)
	{
		String programas="";
		
		for (int i = 0; i < lista_programas.size(); i++)
		{
			switch(i)
			{
				case 0: 
					if(lista_programas.get(i).isChekeado()==1)
						programas = new String(" LIKE \"%CBEF%\"");
					break;
				case 1:
					if(lista_programas.get(i).isChekeado()==1)
					{
						if(programas == "")
							programas = new String(" LIKE \"%CBEI%\"");
						else programas = programas.concat(" OR " + nomTablaProgramas +" LIKE \"%CBEI%\"");
					}
					break;
				case 2:
					if(lista_programas.get(i).isChekeado()==1)
					{
						if(programas == "")
							programas = new String(" LIKE \"%CBEA%\"");
						else programas = programas.concat(" OR " + nomTablaProgramas +" LIKE \"%CBEA%\"");
					}
			}//fin switch				
		}//fin for
		
		return programas;
	}
	
	/**
	 * @return Devuelve la condición para filtrar los centros que tengan TODAS las enseñanzas seleccionadas
	 * */
	public String obtenerCadenaEnsenianzasAND (String nomTablaEnsenianzas, ArrayList<Check_Item> lista_ensenianzas)
	{
		String ensenianzas="";
		
		for (int i = 0; i < lista_ensenianzas.size(); i++)
		{
			switch(i)
			{
				case 0: 
					if(lista_ensenianzas.get(i).isChekeado()==1)
						ensenianzas = new String(" LIKE \"%INF%\"");
					break;
				case 1:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%PRI%\"");
						else ensenianzas = ensenianzas.concat(" AND " + nomTablaEnsenianzas +" LIKE \"%PRI%\"");
					}
					break;
				case 2:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%SEC%\"");
						else ensenianzas = ensenianzas.concat(" AND " + nomTablaEnsenianzas +" LIKE \"%SEC%\"");
					}
					break;
				case 3:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%BAC%\"");
						else ensenianzas = ensenianzas.concat(" AND " + nomTablaEnsenianzas +" LIKE \"%BAC%\"");
					}
					break;
				case 4:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%CFGM%\"");
						else ensenianzas = ensenianzas.concat(" AND " + nomTablaEnsenianzas +" LIKE \"%CFGM%\"");
					}
					break;
				case 5:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%EE%\"");
						else ensenianzas = ensenianzas.concat(" AND " + nomTablaEnsenianzas +" LIKE \"%EE%\"");
					}
					break;
				case 6:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%PCPI%\"");
						else ensenianzas = ensenianzas.concat(" AND " + nomTablaEnsenianzas +" LIKE \"%PCPI%\"");
					}
					break;
				case 7:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%CFGS%\"");
						else ensenianzas = ensenianzas.concat(" AND " + nomTablaEnsenianzas +" LIKE \"%CFGS%\"");
					}
					break;
				case 8:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%ADUL%\"");
						else ensenianzas = ensenianzas.concat(" AND " + nomTablaEnsenianzas +" LIKE \"%ADUL%\"");
					}
					break;
				case 9:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%MUS%\"");
						else ensenianzas = ensenianzas.concat(" AND " + nomTablaEnsenianzas +" LIKE \"%MUS%\"");
					}
					break;
				case 10:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%DANZ%\"");
						else ensenianzas = ensenianzas.concat(" AND " + nomTablaEnsenianzas +" LIKE \"%DANZ%\"");
					}
					break;
				case 11:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%ADR%\"");
						else ensenianzas = ensenianzas.concat(" AND " + nomTablaEnsenianzas +" LIKE \"%ADR%\"");
					}
					break;
				case 12:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%IDI%\"");
						else ensenianzas = ensenianzas.concat(" AND " + nomTablaEnsenianzas +" LIKE \"%IDI%\"");
					}
					break;
				case 13:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%APD%\"");
						else ensenianzas = ensenianzas.concat(" AND " + nomTablaEnsenianzas +" LIKE \"%APD%\"");
					}
					break;				
			}				
		}
		
		return ensenianzas;
	}
	
	/**
	 * @return Devuelve la condición para filtrar los centros que tengan ALGUNAS de las enseñanzas seleccionadas
	 * */
	public String obtenerCadenaEnsenianzasOR (String nomTablaEnsenianzas, ArrayList<Check_Item> lista_ensenianzas)
	{
		String ensenianzas="";
		
		for (int i = 0; i < lista_ensenianzas.size(); i++)
		{
			switch(i)
			{
				case 0: 
					if(lista_ensenianzas.get(i).isChekeado()==1)
						ensenianzas = new String(" LIKE \"%INF%\"");
					break;
				case 1:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%PRI%\"");
						else ensenianzas = ensenianzas.concat(" OR " + nomTablaEnsenianzas +" LIKE \"%PRI%\"");
					}
					break;
				case 2:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%SEC%\"");
						else ensenianzas = ensenianzas.concat(" OR " + nomTablaEnsenianzas +" LIKE \"%SEC%\"");
					}
					break;
				case 3:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%BAC%\"");
						else ensenianzas = ensenianzas.concat(" OR " + nomTablaEnsenianzas +" LIKE \"%BAC%\"");
					}
					break;
				case 4:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%CFGM%\"");
						else ensenianzas = ensenianzas.concat(" OR " + nomTablaEnsenianzas +" LIKE \"%CFGM%\"");
					}
					break;
				case 5:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%EE%\"");
						else ensenianzas = ensenianzas.concat(" OR " + nomTablaEnsenianzas +" LIKE \"%EE%\"");
					}
					break;
				case 6:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%PCPI%\"");
						else ensenianzas = ensenianzas.concat(" OR " + nomTablaEnsenianzas +" LIKE \"%PCPI%\"");
					}
					break;
				case 7:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%CFGS%\"");
						else ensenianzas = ensenianzas.concat(" OR " + nomTablaEnsenianzas +" LIKE \"%CFGS%\"");
					}
					break;
				case 8:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%ADUL%\"");
						else ensenianzas = ensenianzas.concat(" OR " + nomTablaEnsenianzas +" LIKE \"%ADUL%\"");
					}
					break;
				case 9:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%MUS%\"");
						else ensenianzas = ensenianzas.concat(" OR " + nomTablaEnsenianzas +" LIKE \"%MUS%\"");
					}
					break;
				case 10:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%DANZ%\"");
						else ensenianzas = ensenianzas.concat(" OR " + nomTablaEnsenianzas +" LIKE \"%DANZ%\"");
					}
					break;
				case 11:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%ADR%\"");
						else ensenianzas = ensenianzas.concat(" OR " + nomTablaEnsenianzas +" LIKE \"%ADR%\"");
					}
					break;
				case 12:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%IDI%\"");
						else ensenianzas = ensenianzas.concat(" OR " + nomTablaEnsenianzas +" LIKE \"%IDI%\"");
					}
					break;
				case 13:
					if(lista_ensenianzas.get(i).isChekeado()==1)
					{
						if(ensenianzas == "") ensenianzas = new String(" LIKE \"%APD%\"");
						else ensenianzas = ensenianzas.concat(" OR " + nomTablaEnsenianzas +" LIKE \"%APD%\"");
					}
					break;				
			}				
		}
		
		return ensenianzas;
	}
}